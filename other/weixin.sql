/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100114
 Source Host           : 127.0.0.1
 Source Database       : weixin

 Target Server Type    : MariaDB
 Target Server Version : 100114
 File Encoding         : utf-8

 Date: 03/08/2017 07:18:46 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `openid` varchar(64) NOT NULL,
  `headimgurl` varchar(500) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `subscribe` int(32) DEFAULT NULL,
  `nickname` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `sex` int(32) DEFAULT NULL,
  `language` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `subscribe_time` bigint(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `openid` (`openid`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `weixin_user`
-- ----------------------------
DROP TABLE IF EXISTS `weixin_user`;
CREATE TABLE `weixin_user` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `openid` varchar(50) NOT NULL,
  `hb_username` varchar(50) DEFAULT NULL,
  `nick_name` varchar(50) DEFAULT NULL,
  `archived` tinyint(1) DEFAULT NULL,
  `version` int(32) DEFAULT NULL,
  `guid` varchar(50) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `openid` (`openid`),
  UNIQUE KEY `guid` (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
